# wordcount

#### 软件介绍。

WordCount项目是实现一个命令行文本计数统计程序。能正确统计导入的纯英文txt文本中的字符数，单词数，句子数。



#### 使用说明


用法示例

1. -c
     文件的字符数  示例：wc.exe -c file.c [表示返回文件file.c的字符数，并存储在result.txt中]
2. -w
     文件单词总数  示例：wc.exe -w file.c [表示返回文件file.c的单词数，并存储在result.txt中]
3. -l
     文件行数      示例：wc.exe -l file.c [表示返回文件file.c的总行数，并存储在result.txt中]


#### 扩展功能

1.  -s  查找文件的函数
2.  -a  返回文件的空行数
        、返回注释行的行数
        、返回代码的行数

#### 文件列标
![输入图片说明](https://images.gitee.com/uploads/images/2020/1127/183801_02b87016_8316047.png "屏幕截图.png")
#### 运行结果
![输入图片说明](https://images.gitee.com/uploads/images/2020/1127/183908_cc75999c_8316047.png "测试结果.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/1127/183921_fefc175b_8316047.png "测试结果2.png")
